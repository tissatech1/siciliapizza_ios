//
//  DirectionPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 7/9/21.
//

import UIKit
import SideMenu
import ProgressHUD
import SafariServices
import WebKit

class DirectionPage: UIViewController,WKNavigationDelegate, WKUIDelegate  {
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
       
        webView.navigationDelegate = self
        webView.backgroundColor = .clear
        webView.uiDelegate = self
        webView.configuration.dataDetectorTypes = [.link, .phoneNumber]

        let url = URL(string: "https://www.google.com/maps/place/558+S+Delsea+Dr,+Clayton,+NJ+08312,+USA/@39.6481147,-75.0917057,17z/data=!3m1!4b1!4m5!3m4!1s0x89c729c0ba7d8dab:0x22596d804a598cae!8m2!3d39.6481147!4d-75.089517")!
    webView.load(URLRequest(url: url))
    webView.allowsBackForwardNavigationGestures = true
        webView.scrollView.isScrollEnabled = true
       self.webView.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: .new, context: nil)
        
    }
   
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func reload(_ sender: UIButton) {

      //  webView.reload()
        
        if(self.webView.canGoBack) {
                self.webView.goBack()
               }

    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        ProgressHUD.dismiss()
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            if keyPath == "loading" {
                if webView.isLoading {
                    ProgressHUD.show()
                    
                } else {
                    
                    ProgressHUD.dismiss()
                }
            }
        }
    

    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    
    }
    
    ///    ProgressHUD.dismiss()

        func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
            ProgressHUD.show()
           // print(webView.url!)
        }

        func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {

            print("error  - \(error)")
        }

    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
            
            if navigationAction.targetFrame == nil {
                self.webView.load(navigationAction.request)
                }
            return nil
        }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            if let requestUrl = navigationAction.request.url, requestUrl.scheme == "tel" {
                UIApplication.shared.open(requestUrl, options: [:], completionHandler: nil)
                decisionHandler(.cancel)

            } else {
                decisionHandler(.allow)
            }
        }


}
