//
//  FirstShPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 7/9/21.
//

import UIKit
import SideMenu
import ProgressHUD
import SafariServices
import WebKit
class FirstShPage: UIViewController {

    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var orderBtn: UIButton!
    @IBOutlet weak var directionBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)

        callBtn.layer.cornerRadius = 10
        orderBtn.layer.cornerRadius = 10
        directionBtn.layer.cornerRadius = 10
        
        
        
        
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func callClicked(_ sender: Any) {
        
        let phone = "8568819566"
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    @IBAction func orderClicked(_ sender: Any) {
        
        let about = self.storyboard?.instantiateViewController(withIdentifier: "OtherHome") as! OtherHome
        self.navigationController?.pushViewController(about, animated: true)
        
    }
    
    @IBAction func directionClicked(_ sender: Any) {
        
        let about = self.storyboard?.instantiateViewController(withIdentifier: "DirectionPage") as! DirectionPage
        self.navigationController?.pushViewController(about, animated: true)
        
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        ProgressHUD.dismiss()
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
}
