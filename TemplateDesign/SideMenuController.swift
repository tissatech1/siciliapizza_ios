//
//  SideMenuController.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/25/21.
//

import UIKit
import Alamofire

class SideMenuController: UIViewController,UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var sidemenuView: UIView!
    @IBOutlet weak var sidetableview: UITableView!

    var lablename = NSArray()
    var imagesarr = NSArray()
   
    override func viewDidLoad() {
        super.viewDidLoad()

         lablename = ["Home","About Us","Contact Us"]
         imagesarr = ["home.png","info.png","email.png"]
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        sidemenuView.layer.cornerRadius = 8
        
        sidetableview.register(UINib(nibName: "SideMenuTVCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
       
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return lablename.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SideMenuTVCell

        cell.selectionStyle = .none
        
        cell.cellouter.layer.cornerRadius = 8
//        cell.cellouter.layer.shadowColor = UIColor.lightGray.cgColor
//        cell.cellouter.layer.shadowOpacity = 1
//        cell.cellouter.layer.shadowOffset = .zero
//        cell.cellouter.layer.shadowRadius = 3
        
//        cell.cellouter.layer.borderWidth = 0.5
//        cell.cellouter.layer.borderColor = UIColor.gray.cgColor
        
        cell.celllable.text = lablename[indexPath.row] as? String
        cell.cellimage.image = UIImage(named: imagesarr[indexPath.row] as! String)
        

        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        
            return 54
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let about = self.storyboard?.instantiateViewController(withIdentifier: "FirstShPage") as! FirstShPage
            self.navigationController?.pushViewController(about, animated: true)
            
        }else if indexPath.row == 1 {
            let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutusPage") as! AboutusPage
            self.navigationController?.pushViewController(about, animated: true)
            
        }else{
            let contact = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsPage") as! ContactUsPage
            self.navigationController?.pushViewController(contact, animated: true)
            
        }
       
    }
    
  
    @IBAction func backClicked(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)

    }

}
